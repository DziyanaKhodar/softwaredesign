package com.example.newuser.application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.newuser.application.DataAccess.DataAccess;
import com.example.newuser.application.viewModels.ProfileViewModel;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, AboutFragment.OnFragmentInteractionListener {

    Button loginBtn,registrationBtn;
    ProfileViewModel viewModel;
    EditText loginEd,passwordEd;
    AboutFragment aboutFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loginEd=(EditText)findViewById(R.id.loginEd);
        passwordEd=(EditText)findViewById(R.id.loginPass);
        loginBtn=(Button)findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);
        registrationBtn=(Button)findViewById(R.id.registrationBtn);
        registrationBtn.setOnClickListener(this);
        viewModel = new ProfileViewModel(DataAccess.getDataAccess().loadUserInfo());

    }

    @Override
    public void onBackPressed()
    {
        if (aboutFragment!=null)
        {
            setInvisible(View.VISIBLE);
            super.onBackPressed();
            aboutFragment=null;
        }
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.option_about:
                if (aboutFragment==null)
                    return true;
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.login_container,aboutFragment =new AboutFragment());
                ft.addToBackStack(null);
                ft.commit();
                setInvisible(View.INVISIBLE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void setInvisible(int set)
    {
        loginBtn.setVisibility(set);
        registrationBtn.setVisibility(set);
        loginEd.setVisibility(set);
        passwordEd.setVisibility(set);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.loginBtn:
                login();
                break;
            case R.id.registrationBtn:
                registrate();
                break;
        }
    }
    private void login()
    {
        Log.d("logger",viewModel.getLogin()+" "+viewModel.getPassword()+" "+viewModel.getEmail());
        String login=loginEd.getText().toString(),password = passwordEd.getText().toString();
        if (login.trim().isEmpty()||password.trim().isEmpty())
        {
            Toast.makeText(this,R.string.empty_login_or_password,Toast.LENGTH_LONG).show();
            return;
        }
        if (viewModel.getLogin()==null||viewModel.getPassword()==null)
        {
            Toast.makeText(this,R.string.error_login,Toast.LENGTH_LONG).show();
            return;
        }

        if (login.contains(viewModel.getLogin())&&password.contains(viewModel.getPassword()))
        {
            Intent intent = new Intent(this,Main2Activity.class);
            startActivity(intent);
            this.finish();
        }
        else
        {
            Toast.makeText(this,R.string.error_login,Toast.LENGTH_LONG).show();
            return;
        }
    }
    private void registrate()
    {
        String login=loginEd.getText().toString(),password = passwordEd.getText().toString();
        if (login.trim().isEmpty()||password.trim().isEmpty())
        {
            Toast.makeText(this,R.string.empty_login_or_password,Toast.LENGTH_LONG).show();
            return;
        }
        viewModel.setLogin(login);
        viewModel.setPassword(password);
        DataAccess.getDataAccess().saveUserInfo(viewModel.getUser());
        Intent intent = new Intent(this,Main2Activity.class);
        startActivity(intent);
        this.finish();
    }
    @Override
    public void onFragmentInteraction(Uri uri){}


}

package com.example.newuser.application;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.example.newuser.application.DataAccess.DataAccess;
import com.example.newuser.application.databinding.FragmentEditUserAccountBinding;
import com.example.newuser.application.viewModels.ProfileViewModel;


import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;


public class EditUserAccountFragment extends Fragment{

    private FragmentEditUserAccountBinding binding;
    private final static int CAMERA_PERM = 3;
    private final static int CAMERA = 1;
    private final static String AVATAR = "avatar";
    private ProfileViewModel _viewModel;
    private boolean _avatarChanged;

    private OnFragmentInteractionListener mListener;

    private byte[] encodeBmp(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    private Bitmap decodeBmp(byte[] arr)
    {
        return BitmapFactory.decodeByteArray(arr, 0, arr.length);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        try {
            Bitmap bmp = ((BitmapDrawable) ((ImageView) getView().findViewById(R.id.avatar)).getDrawable()).getBitmap();
            outState.putByteArray(AVATAR, encodeBmp(bmp));
            }
            catch (Exception ex){}
        super.onSaveInstanceState(outState);
    }

    public static EditUserAccountFragment newInstance(String param1, String param2) {
        EditUserAccountFragment fragment = new EditUserAccountFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
       _viewModel = new ProfileViewModel(DataAccess.getDataAccess().loadUserInfo());
       binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_user_account, container, false);
       binding.setUser(_viewModel);
        if (savedInstanceState != null&&savedInstanceState.getByteArray(AVATAR)!=null) {


            Bitmap bmp = decodeBmp(savedInstanceState.getByteArray(AVATAR));
            ((ImageView) getView().findViewById(R.id.avatar)).setImageBitmap(bmp);
        }



       return binding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        getView().findViewById(R.id.load_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGallery();
            }
        });

        getView().findViewById(R.id.take_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhotoFromCamera();
            }
        });

        getView().findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(_avatarChanged)
                {
                    BitmapDrawable bitmapImage = (BitmapDrawable)((ImageView)getView().findViewById(R.id.avatar)).getDrawable();
                    String path = DataAccess.getDataAccess().saveImage(bitmapImage.getBitmap());
                    _viewModel.setPhotoPath(path);
                }

                DataAccess.getDataAccess().saveUserInfo(_viewModel.getUser());
                mListener.userAccount();
            }
        });
    }

    private void startGallery() {
        ImagePicker.create(this)
                .single()
                .start();
    }

    private void takePhotoFromCamera() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[] {Manifest.permission.CAMERA}, CAMERA_PERM);
        }
        else
        {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults){
        switch (requestCode)
        {
            case CAMERA_PERM:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, CAMERA);
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            Image image = ImagePicker.getFirstImageOrNull(data);
            String path = image.getPath();
            Glide.with(getContext())
                    .load(path)
                    .into((ImageView)getView().findViewById(R.id.avatar));
            _avatarChanged = true;
            return;
        }

        if(resultCode == RESULT_OK) {
            switch (requestCode){
                case CAMERA:
                    Bitmap bitmap = (Bitmap)data.getExtras().get("data");

                    if (bitmap != null) {
                        ((ImageView) getView().findViewById(R.id.avatar)).setImageBitmap(bitmap);
                        _avatarChanged = true;
                    }
            }
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();
        mListener = null;
    }
    @Override
    public void onPause()
    {

        super.onPause();
    }
    public void save()
    {
        if(_avatarChanged)
        {
            BitmapDrawable bitmapImage = (BitmapDrawable)((ImageView)getView().findViewById(R.id.avatar)).getDrawable();
            String path = DataAccess.getDataAccess().saveImage(bitmapImage.getBitmap());
            _viewModel.setPhotoPath(path);
        }

        DataAccess.getDataAccess().saveUserInfo(_viewModel.getUser());

    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        void userAccount();
    }
}

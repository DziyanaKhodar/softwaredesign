package com.example.newuser.application.DataAccess;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

public class DataAccess implements IDataAccess {

    private static DataAccess _dataAccess;
    private static String _directoryName = "softwaredesign";
    private static String _userInfoFileName = "user.txt";

    public Bitmap loadImage(String url)
    {
        if(url == null)
        {
            return null;
        }
        File imgFile = new File(url);

        if (imgFile.exists()) {

            return BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        return null;
    }

    public String saveImage(Bitmap bmImg)
    {
        try
        {
            String imageName = "" + ".jpg";
            String path = getDirectoryAbsolutePath() + "/" + imageName;
            File file = new File(path);
            if(!file.exists())
            {
                file.createNewFile();
            }

            FileOutputStream out = new FileOutputStream(file);

            bmImg.compress(Bitmap.CompressFormat.JPEG, 90, out);

            out.flush();

            out.close();

            return path;

        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }
    }

    public static IDataAccess getDataAccess()
    {
        if(_dataAccess == null)
        {
            _dataAccess = new DataAccess();
        }

        return _dataAccess;
    }

    public void saveUserInfo(User user)
    {
        try
        {
            String path = getDirectoryAbsolutePath() + "/" + _userInfoFileName;

            File file = new File(path);
            if(!file.exists())
            {
                file.createNewFile();
            }

            String lineSep = System.getProperty("line.separator");

            FileWriter fw = new FileWriter(path);
            fw.write(user.getName());
            fw.append(lineSep + user.getLastName());
            fw.append(lineSep + user.getEmail());
            fw.append(lineSep + user.getPhone());
            fw.append(lineSep + user.getPhotoPath());
            fw.append(lineSep + user.getLogin());
            fw.append(lineSep + user.getPassword());

            fw.flush();
            fw.close();

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public User loadUserInfo()
    {
        try
        {

            User user = new User();
            String path =  getDirectoryAbsolutePath() + "/" + _userInfoFileName;

            BufferedReader br = new BufferedReader(new FileReader(path));

            user.setName(br.readLine());
            user.setLastName(br.readLine());
            user.setEmail(br.readLine());
            user.setPhone(br.readLine());
            user.setPhotoPath(br.readLine());
            user.setLogin(br.readLine());
            user.setPassword(br.readLine());

            br.close();
            return user;

        } catch (Exception e) {

            User user = new User();
            user.setName("Example");
            user.setLastName("Example");
            user.setEmail("example@example.com");
            user.setPhone("3333");
            user.setLogin("admin");
            user.setPassword("admin");
            e.printStackTrace();
            return user;
        }
    }

    private String getDirectoryAbsolutePath()
    {

        String extStDir = android.os.Environment.getExternalStorageDirectory()
                .toString();

        File directory = new File(extStDir + "/" + _directoryName);


        if (!directory.exists())
            directory.mkdirs();

        return directory.getAbsolutePath();
    }

}

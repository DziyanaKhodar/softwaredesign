package com.example.newuser.application;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.newuser.application.DataAccess.DataAccess;
import com.example.newuser.application.databinding.FragmentUserAccountBinding;
import com.example.newuser.application.viewModels.ProfileViewModel;


public class UserAccountFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    public UserAccountFragment()
    {
        // Required empty public constructor
    }

    public static UserAccountFragment newInstance(String param1, String param2) {
        UserAccountFragment fragment = new UserAccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ProfileViewModel viewModel = new ProfileViewModel(DataAccess.getDataAccess().loadUserInfo());
        FragmentUserAccountBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_account, container, false);
        binding.setUser(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Button editButton = (Button)getView().findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.editUser(null);
            }
        });
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        void editUser(String str);
    }
}

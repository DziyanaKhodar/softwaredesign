package com.example.newuser.application.viewModels;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import com.example.newuser.application.BR;
import com.example.newuser.application.DataAccess.DataAccess;
import com.example.newuser.application.DataAccess.IDataAccess;
import com.example.newuser.application.DataAccess.User;



import java.io.File;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;

//  @InverseBindingMethods(
//          {@InverseBindingMethod(type = android.widget.ImageView.class, attribute = "imageUrl", method = "getImageUrl")})
public class ProfileViewModel extends BaseObservable {

    private User _user;

    public ProfileViewModel(User user)
    {
        super();
        _user = user;//DataAccess.getDataAccess().loadUserInfo();
    }


    public User getUser()
    {
        return  _user;
    }

    @Bindable
    public String getLogin()
    {
        return _user.getLogin();
    }

    @Bindable
    public String getPassword()
    {
        return _user.getPassword();
    }

    @Bindable
    public String getName()
    {
        return _user.getName();
    }

    @Bindable
    public String getLastName()
    {
        return _user.getLastName();
    }

    @Bindable
    public String getEmail()
    {
        return _user.getEmail();
    }

    @Bindable
    public String getPhone()
    {
        return _user.getPhone();
    }

    @BindingAdapter("imageUrl")//(value = {"imageUrl", "defaultImageRes"}, requireAll = false)
    public static void loadImage(ImageView view, String url)//, String defaultImageRes)
    {
        Bitmap bitmap = DataAccess.getDataAccess().loadImage(url);
        if(bitmap != null)
        {
            view.setImageBitmap(bitmap);
        }
//        else
//        {
//            Context context = view.getContext();
//            int id = context.getResources().getIdentifier("noimage.png" , "drawable", context.getPackageName());
//            view.setImageResource(id);
//        }
    }

    public void setLogin(String value)
    {
        _user.setLogin(value);
        notifyPropertyChanged(BR.name);
    }

    public void setPassword(String value)
    {
        _user.setPassword(value);
        notifyPropertyChanged(BR.name);
    }

    public void setName(String value)
    {
        _user.setName(value);
        notifyPropertyChanged(BR.name);
    }

    public void setLastName(String value)
    {
        _user.setLastName(value);
        notifyPropertyChanged(BR.lastName);
    }

    public void setEmail(String value)
    {
        _user.setEmail(value);
        notifyPropertyChanged(BR.email);
    }

    public void setPhone(String value)
    {
        _user.setPhone(value);
        notifyPropertyChanged(BR.phone);
    }

    public String getImageUrl()
    {
        return _user.getPhotoPath();
    }

    @Bindable
    public String getPhotoPath()
    {
        return _user.getPhotoPath();
    }

    public void setPhotoPath(String value)
    {
        _user.setPhotoPath(value);
    }
}

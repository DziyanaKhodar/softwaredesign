package com.example.newuser.application.DataAccess;

public class User {
    private String _name;
    private String _lastName;
    private String _email;
    private String _phone;
    private String _photoPath;
    private String _login;
    private String _password;

    public String getName()
    {
        return _name;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public String getEmail()
    {
        return _email;
    }

    public String getPhone()
    {
        return _phone;
    }

    public String getPhotoPath()
    {
        return _photoPath;
    }

    public void setName(String value)
    {
        _name = value;
    }

    public void setLastName(String value)
    {
        _lastName = value;
    }

    public void setEmail(String value)
    {
        _email = value;
    }

    public void setPhone(String value)
    {
        _phone = value;
    }

    public void setPhotoPath(String value)
    {
        _photoPath = value;
    }

    public void setLogin(String value)
    {
        _login = value;
    }

    public String getLogin()
    {
        return _login;
    }

    public void setPassword(String value)
    {
        _password = value;
    }

    public String getPassword()
    {
        return _password;
    }
}

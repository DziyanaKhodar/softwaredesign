package com.example.newuser.application;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutFragment extends Fragment {

    public static final int READ_PHONE_STATE_REQUEST = 1;

    private OnFragmentInteractionListener mListener;

    public AboutFragment()
    {

    }

    public static AboutFragment newInstance(String param1, String param2) {
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void setDeviceIdToTextView()
    {
        String deviceId = getDeviceId();
        TextView deviceIdTextView= (TextView) getView().findViewById(R.id.deviceIdTextView);
        deviceIdTextView.setText(deviceId);
    }

    private void setVersionNameToTextView()
    {
        String versionName = getVersionName();
        TextView versionTextView = (TextView) getView().findViewById(R.id.versionTextView);
        versionTextView.setText(versionName);
    }

    private void setDeviceId()
    {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED){

            RequestDeviceIdPermission();
        } else {
            setDeviceIdToTextView();
        }
    }

    private void RequestDeviceIdPermission()
    {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.READ_PHONE_STATE},
                READ_PHONE_STATE_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_PHONE_STATE_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setDeviceIdToTextView();
                } else {
                    if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_PHONE_STATE))
                    {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                        alertBuilder.setCancelable(false);
                        alertBuilder.setTitle("Permission is required");
                        alertBuilder.setMessage("Read phone state permission is required in order to display your device id");
                        alertBuilder.setPositiveButton( R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                RequestDeviceIdPermission();
                            }
                        });
                        AlertDialog alertDialog = alertBuilder.create();
                        alertDialog.show();
                    }
                    else {
                        RequestDeviceIdPermission();
                    }
                }
            }
        }
    }

    private String getVersionName()
    {
        return BuildConfig.VERSION_NAME;
    }

    private String getDeviceId()
    {
        try {
            TelephonyManager telephonyManager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            return telephonyManager.getDeviceId();
        }
        catch (SecurityException ex)
        {
            return "No device ID";
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        setDeviceId();
        setVersionNameToTextView();
    }
}

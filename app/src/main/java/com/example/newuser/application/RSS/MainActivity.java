package com.example.newuser.application.RSS;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.newuser.application.R;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;


public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    EditText address;
    Button btn_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rss_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        address = (EditText)findViewById(R.id.rss_main_Ed);
        btn_search = (Button)findViewById(R.id.rss_btn);
        ReadRss readRss = new ReadRss(this, recyclerView,"http://www.sciencemag.org/rss/news_current.xml");
        readRss.execute();
    }

}
package com.example.newuser.application;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import com.example.newuser.application.RSS.MainActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.View;

import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;
import android.view.MenuItem;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        UserAccountFragment.OnFragmentInteractionListener,
        AboutFragment.OnFragmentInteractionListener,
        BlankFragment.OnFragmentInteractionListener,
        EditUserAccountFragment.OnFragmentInteractionListener
        {

    private final int WRITE_STORAGE_PERM = 1;

    UserAccountFragment userAccountFragment;
    AboutFragment aboutFragment;
    BlankFragment blankFragment;
    EditUserAccountFragment editUserAccountFragment;

    NavigationView navigationView;
    FragmentTransaction ft;
    int selected_fragment =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        userAccountFragment = new UserAccountFragment();
        aboutFragment = new AboutFragment();
        blankFragment = new BlankFragment();
        editUserAccountFragment = new EditUserAccountFragment();
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container,userAccountFragment);
        ft.commit();

        if ((ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)){

            RequestStoragePermission();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(selected_fragment==1)
            {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        editUserAccountFragment.save();
                        Main2Activity.super.onBackPressed();
                    }
                });
                builder.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Main2Activity.super.onBackPressed();
                    }
                });
                builder.setTitle(R.string.dialog_title);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        ft = getSupportFragmentManager().beginTransaction();

        if (id == R.id.edit_user) {
            selected_fragment=0;
            ft.replace(R.id.container,userAccountFragment);
            ft.addToBackStack(null);
            ft.commit();
        } else if (id == R.id.blank) {
            selected_fragment=0;
            ft.replace(R.id.container,blankFragment);
            ft.addToBackStack(null);
            ft.commit();
        } else if (id == R.id.about) {
            selected_fragment=0;
            ft.replace(R.id.container,aboutFragment);
            ft.addToBackStack(null);
            ft.commit();
        } else if(id==R.id.rss)
        {
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void RequestStoragePermission()
    {
        ActivityCompat.requestPermissions(
                this,
                new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE},
                WRITE_STORAGE_PERM);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        switch (requestCode) {
            case WRITE_STORAGE_PERM: {
                if (!(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {

                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) )
                    {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                        alertBuilder.setCancelable(false);
                        alertBuilder.setTitle("Permission is required");
                        alertBuilder.setMessage("Storage permission is required in order to save data about your account");
                        alertBuilder.setPositiveButton( R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                RequestStoragePermission();
                            }
                        });
                        AlertDialog alertDialog = alertBuilder.create();
                        alertDialog.show();
                    }
                    else {
                        RequestStoragePermission();
                    }
                }
            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void editUser(String str){
        selected_fragment=1;
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container,editUserAccountFragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    @Override
    public void userAccount()
    {
        selected_fragment=0;
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container,userAccountFragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}

package com.example.newuser.application.DataAccess;

import android.graphics.Bitmap;

public interface IDataAccess {
    User loadUserInfo();
    void saveUserInfo(User user);
    Bitmap loadImage(String url);
    String saveImage(Bitmap img);
}
